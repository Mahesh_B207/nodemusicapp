$.getScript("'//cdnjs.cloudflare.com/ajax/libs/wavesurfer.js/2.0.6/wavesurfer.min.js'", function() {
   alert("Script loaded but not necessarily executed.");
});
var wavesurfer = WaveSurfer.create({
container: '#waveform',
waveColor: 'red',
progressColor: 'purple'});
wavesurfer.on('ready', function () {
wavesurfer.play();
});

wavesurfer.on('scroll', function () {
});

wavesurfer.on('finish', function () {
if(audio1 == true)
{
wavesurfer.play();
}
});
var audio1 = false;

var wavesurfer2 = WaveSurfer.create({
container: '#waveform2',
waveColor: 'red',
progressColor: 'purple'});
wavesurfer2.on('ready', function () {
wavesurfer2.play();
});

wavesurfer2.on('finish', function () {
if(audio2 == true)
{
wavesurfer2.play();
}
});
var audio2 = false;

var wavesurfer3 = WaveSurfer.create({
container: '#waveform3',
waveColor: 'red',
progressColor: 'purple'});
wavesurfer3.on('ready', function () {
wavesurfer3.play();
});

wavesurfer3.on('finish', function () {
if(audio3 == true)
{
wavesurfer3.play();
}
});
var audio3 = false;

var andamgaAudio = new Audio('http://localhost:3000/music?id=andamga.mp3', type='audio/mp3');
var bAudio = new Audio('http://localhost:3000/music?id=CarHandbrakeON.wav', type='audio/wav');

var value1 = document.getElementById("DropDown1").value;
var tempAudio = new Audio('http://localhost:3000/music?id=' + value1 + '.wav', type='audio/wav');
var value2 = document.getElementById("DropDown2").value;
var tempAudio2 = new Audio('http://localhost:3000/music?id=' + value2 + '.wav', type='audio/wav');
var value3 = document.getElementById("DropDown3").value;
var tempAudio3 = new Audio('http://localhost:3000/music?id=' + value3 + '.wav', type='audio/wav');
function playAudio() {
andamgaAudio.play();
}
function pauseAudio() {
andamgaAudio.pause();
}

function changePlayImage() {
if (document.getElementById("PlayImg").src == "http://localhost:3000/images?id=play.jpg") 
{
document.getElementById("imgClickAndChange").src = "http://localhost:3000/images?id=play.jpg";
document.getElementById("imgClickAndChange2").src = "http://localhost:3000/images?id=play.jpg";
document.getElementById("imgClickAndChange3").src = "http://localhost:3000/images?id=play.jpg";
/*tempAudio.play();
tempAudio.loop = true; 
tempAudio2.play();
tempAudio2.loop = true; 
tempAudio3.play();
tempAudio3.loop = true; */
document.getElementById("PlayImg").src = "http://localhost:3000/images?id=pause.png";
changeImage1();
changeImage2();
changeImage3();
}
else
{
document.getElementById("PlayImg").src = "http://localhost:3000/images?id=play.jpg";
document.getElementById("imgClickAndChange").src = "http://localhost:3000/images?id=play.jpg";
document.getElementById("imgClickAndChange2").src = "http://localhost:3000/images?id=play.jpg";
document.getElementById("imgClickAndChange3").src = "http://localhost:3000/images?id=play.jpg";
tempAudio.loop = false; 
tempAudio.pause();
tempAudio2.loop = false; 
tempAudio2.pause();
tempAudio3.loop = false; 
tempAudio3.pause(); 
wavesurfer.pause();
wavesurfer2.pause();
wavesurfer3.pause();
}
}

function SetAllVolume(val)
{
	SetVolume1(val);
	SetVolume2(val);
	SetVolume3(val);
	metronomeAudio.volume = val/100;
}
function SetVolume1(val)
{
tempAudio.volume = val / 100;
wavesurfer.setVolume(val * 0.01);
}
function SetVolume2(val)
{
tempAudio2.volume = val / 100;
wavesurfer2.setVolume(val * 0.01);
}
function SetVolume3(val)
{
tempAudio3.volume = val / 100;
wavesurfer3.setVolume(val * 0.01);

}

function SetSpeed(val)
{
	tempAudio.playbackRate = val;
	tempAudio2.playbackRate = val;
	tempAudio3.playbackRate = val;
	wavesurfer.setPlaybackRate(val);
	wavesurfer2.setPlaybackRate(val);
	wavesurfer3.setPlaybackRate(val);
}

function dropDown1Changed(){
tempAudio.pause();
wavesurfer.pause();
value1 = document.getElementById("DropDown1").value;
tempAudio = new Audio('http://localhost:3000/music?id=' + value1 + '.wav', type='audio/wav');
if (document.getElementById("imgClickAndChange").src != "http://localhost:3000/images?id=play.jpg") 
{
tempAudio.play();
tempAudio.loop = true; 
tempAudio.muted = true;
wavesurfer.load(tempAudio.src);
wavesurfer.play();
}
}
function dropDown2Changed(){
tempAudio2.pause();
tempAudio2.loop = true;
wavesurfer2.pause(); 
value2 = document.getElementById("DropDown2").value;
tempAudio2 = new Audio('http://localhost:3000/music?id=' + value2 + '.wav', type='audio/wav');
if (document.getElementById("imgClickAndChange2").src != "http://localhost:3000/images?id=play.jpg") 
{
tempAudio2.play();
tempAudio2.loop = true;
tempAudio2.muted = true;
wavesurfer2.load(tempAudio2.src);
wavesurfer2.play();
}
}
function dropDown3Changed(){
tempAudio3.pause();
tempAudio3.loop = true;
wavesurfer3.pause(); 
value3 = document.getElementById("DropDown3").value;
tempAudio3 = new Audio('http://localhost:3000/music?id=' + value3 + '.wav', type='audio/wav');
if (document.getElementById("imgClickAndChange3").src != "http://localhost:3000/images?id=play.jpg") 
{
tempAudio3.play();
tempAudio3.loop = true; 
tempAudio3.muted = true;
wavesurfer3.load(tempAudio3.src);
wavesurfer3.play();
}
}

function changeImage1() {
if (document.getElementById("imgClickAndChange").src == "http://localhost:3000/images?id=play.jpg") 
{
if (document.getElementById("imgClickAndChange2").src == "http://localhost:3000/images?id=pause.png" && document.getElementById("imgClickAndChange3").src == "http://localhost:3000/images?id=pause.png")
{
document.getElementById("PlayImg").src = "http://localhost:3000/images?id=pause.png";
}
document.getElementById("imgClickAndChange").src = "http://localhost:3000/images?id=pause.png";
tempAudio.play();
tempAudio.loop = true; 
tempAudio.muted = true;
wavesurfer.load(tempAudio.src);
wavesurfer.play();
audio1 = true;
}
else 
{
if (document.getElementById("imgClickAndChange2").src == "http://localhost:3000/images?id=play.jpg" && document.getElementById("imgClickAndChange3").src == "http://localhost:3000/images?id=play.jpg")
{
document.getElementById("PlayImg").src = "http://localhost:3000/images?id=play.jpg";
}
audio1 = false;
document.getElementById("imgClickAndChange").src = "http://localhost:3000/images?id=play.jpg";
tempAudio.loop = false; 
tempAudio.pause();
tempAudio.currentTime = 0;
tempAudio.stop();
wavesurfer.pause();
}
}
function changeImage2() {
if (document.getElementById("imgClickAndChange2").src == "http://localhost:3000/images?id=play.jpg") 
{
if (document.getElementById("imgClickAndChange").src == "http://localhost:3000/images?id=pause.png" && document.getElementById("imgClickAndChange3").src == "http://localhost:3000/images?id=pause.png")
{
document.getElementById("PlayImg").src = "http://localhost:3000/images?id=pause.png";
}
document.getElementById("imgClickAndChange2").src = "http://localhost:3000/images?id=pause.png";
tempAudio2.play();
tempAudio2.loop = true; 
tempAudio2.muted = true;
wavesurfer2.load(tempAudio2.src);
wavesurfer2.play();
audio2 = true;
}
else 
{
if (document.getElementById("imgClickAndChange").src == "http://localhost:3000/images?id=play.jpg" && document.getElementById("imgClickAndChange3").src == "http://localhost:3000/images?id=play.jpg")
{
document.getElementById("PlayImg").src = "http://localhost:3000/images?id=play.jpg";
}
audio2 = false;
document.getElementById("imgClickAndChange2").src = "http://localhost:3000/images?id=play.jpg";
tempAudio2.loop = false; 
tempAudio2.pause();
tempAudio2.currentTime = 0;
tempAudio2.stop();
wavesurfer2.pause();
}
}
function changeImage3() {
if (document.getElementById("imgClickAndChange3").src == "http://localhost:3000/images?id=play.jpg") 
{
if (document.getElementById("imgClickAndChange").src == "http://localhost:3000/images?id=pause.png" && document.getElementById("imgClickAndChange2").src == "http://localhost:3000/images?id=pause.png")
{
document.getElementById("PlayImg").src = "http://localhost:3000/images?id=pause.png";
}
document.getElementById("imgClickAndChange3").src = "http://localhost:3000/images?id=pause.png";
tempAudio3.play();
tempAudio3.loop = true; 
tempAudio3.muted = true;
wavesurfer3.load(tempAudio3.src);
wavesurfer3.play();
audio3 = true;
}
else 
{
if (document.getElementById("imgClickAndChange").src == "http://localhost:3000/images?id=play.jpg" && document.getElementById("imgClickAndChange2").src == "http://localhost:3000/images?id=play.jpg")
{
document.getElementById("PlayImg").src = "http://localhost:3000/images?id=play.jpg";
}
audio3 = false;
document.getElementById("imgClickAndChange3").src = "http://localhost:3000/images?id=play.jpg";
tempAudio3.loop = false; 
tempAudio3.pause();
tempAudio3.currentTime = 0;
tempAudio3.stop();
wavesurfer3.pause();
}
}

var metronomeAudio = new Audio('http://localhost:3000/music?id=metronomeclicker.mp3', type='audio/mp3');

function changeMetronomeImage() {
if (document.getElementById("metroImage").src == "http://localhost:3000/images?id=play.jpg") 
{
metronomeAudio.play();
metronomeAudio.loop = true;
document.getElementById("metroImage").src = "http://localhost:3000/images?id=pause.png";

}
else 
{
metronomeAudio.pause();
metronomeAudio.currentTime = 0;
document.getElementById("metroImage").src = "http://localhost:3000/images?id=play.jpg";
}
}