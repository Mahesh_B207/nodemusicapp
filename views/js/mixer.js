var audio = new Audio('http://localhost:3000/music?id=cliffrichard.wav', type='audio/wav');
var vocalOnlyAudio = new Audio('http://localhost:3000/music?id=editedvocalonlycliffrichard.wav', type='audio/wav');
var video = document.getElementById('vid');
var value1 = document.getElementById("DropDown1").value;
var tempAudio = new Audio('http://localhost:3000/music?id=' + value1 + '.wav', type='audio/wav');
var value2 = document.getElementById("DropDown2").value;
var tempAudio2 = new Audio('http://localhost:3000/music?id=' + value2 + '.wav', type='audio/wav');
var value3 = document.getElementById("DropDown3").value;
var tempAudio3 = new Audio('http://localhost:3000/music?id=' + value3 + '.wav', type='audio/wav');
video.onpause = function(){ audio.pause();vocalOnlyAudio.pause();};
video.onplay = function()
{
//var source = context.createBufferSource(); // creates a sound source
//source.buffer = audio;                    // tell the source which sound to play
//source.connect(context.destination);       // connect the source to the context's destination (the speakers)
//source.start(0);    
//var source = context.createMediaElementSource(audio);
//source.play();
audio.play();
};
video.ontimeupdate = function() 
{audio.currentTime = video.currentTime; 
vocalOnlyAudio.currentTime = video.currentTime; 
 if(video.currentTime >=document.getElementById("LoopEnd").value)
  {
   video.currentTime=document.getElementById("LoopStart").value;
  }
 if (video.currentTime <=document.getElementById("LoopStart").value)
 {
   video.currentTime=document.getElementById("LoopStart").value;
 }
 if(document.getElementById("PlayImg").src=="http://localhost:3000/images?id=pause.png")
  {
    audio.pause();
    vocalOnlyAudio.play();
  }
}
// Load Video
var request = new XMLHttpRequest();
request.open('GET', 'http://localhost:3000/music?id=wedonttalkanymore.mp4',type='video/mp4', true);
request.responseType = 'blob';

request.onload = function() {
// Onload is triggered even on 404
// so we need to check the status code
if (this.status === 200) {
  var videoBlob = this.response;
  var vid = URL.createObjectURL(videoBlob); // IE10+
  // Video is now downloaded
  // and we can set it as source on the video element
  video.src = vid;
}
}
request.onerror = function() {
// Error
}
request.send();
function SetVolume(val)
{
audio.volume = val / 100;
vocalOnlyAudio.volume = val/100;
}

function SetSpeed(val)
{
	 audio.playbackRate = val;
	 video.playbackRate = val;
	 vocalOnlyAudio.playbackRate = val;
}

function changePlayImage() {
if (document.getElementById("PlayImg").src == "http://localhost:3000/images?id=play.jpg") 
{
	document.getElementById("PlayImg").src = "http://localhost:3000/images?id=pause.png";
	audio.pause();
	if (!video.paused){vocalOnlyAudio.play();}
	vocalOnlyAudio.currentTime = audio.currentTime;
}
else
{
	document.getElementById("PlayImg").src = "http://localhost:3000/images?id=play.jpg";
	vocalOnlyAudio.pause();
	if (!video.paused){audio.play();}
	audio.currentTime = vocalOnlyAudio.currentTime; 
}
}

function dropDown1Changed()
{
tempAudio.pause();
value1 = document.getElementById("DropDown1").value;
tempAudio = new Audio('http://localhost:3000/music?id=' + value1 + '.wav', type='audio/wav');
if (document.getElementById("imgClickAndChange").src != "http://localhost:3000/images?id=play.jpg") 
{
tempAudio.play();
tempAudio.loop = true; 
}
}
function changeImage1() {
if (document.getElementById("imgClickAndChange").src == "http://localhost:3000/images?id=play.jpg") 
{
document.getElementById("imgClickAndChange").src = "http://localhost:3000/images?id=pause.png";
tempAudio.play();
tempAudio.loop = true; 
}
else 
{
document.getElementById("imgClickAndChange").src = "http://localhost:3000/images?id=play.jpg";
tempAudio.loop = false; 
tempAudio.pause();
tempAudio.currentTime = 0;
tempAudio.stop();
}
}
function SetVolume1(val)
{
tempAudio.volume = val / 100;
}


function dropDown2Changed()
{
tempAudio2.pause();
value2 = document.getElementById("DropDown2").value;
tempAudio2 = new Audio('http://localhost:3000/music?id=' + value2 + '.wav', type='audio/wav');
if (document.getElementById("imgClickAndChange2").src != "http://localhost:3000/images?id=play.jpg") 
{
tempAudio2.play();
tempAudio2.loop = true; 
}
}
function changeImage2() {
if (document.getElementById("imgClickAndChange2").src == "http://localhost:3000/images?id=play.jpg") 
{
document.getElementById("imgClickAndChange2").src = "http://localhost:3000/images?id=pause.png";
tempAudio2.play();
tempAudio2.loop = true; 
}
else 
{
document.getElementById("imgClickAndChange2").src = "http://localhost:3000/images?id=play.jpg";
tempAudio2.loop = false; 
tempAudio2.pause();
tempAudio2.currentTime = 0;
tempAudio2.stop();
}
}
function SetVolume2(val)
{
tempAudio2.volume = val / 100;
}

function dropDown3Changed()
{
tempAudio3.pause();
value3 = document.getElementById("DropDown3").value;
tempAudio3 = new Audio('http://localhost:3000/music?id=' + value3 + '.wav', type='audio/wav');
if (document.getElementById("imgClickAndChange3").src != "http://localhost:3000/images?id=play.jpg") 
{
tempAudio3.play();
tempAudio3.loop = true; 
}
}
function changeImage3() {
if (document.getElementById("imgClickAndChange3").src == "http://localhost:3000/images?id=play.jpg") 
{
document.getElementById("imgClickAndChange3").src = "http://localhost:3000/images?id=pause.png";
tempAudio3.play();
tempAudio3.loop = true; 
}
else 
{
document.getElementById("imgClickAndChange3").src = "http://localhost:3000/images?id=play.jpg";
tempAudio3.loop = false; 
tempAudio3.pause();
tempAudio3.currentTime = 0;
tempAudio3.stop();
}
}
function SetVolume3(val)
{
tempAudio3.volume = val / 100;
}

function resetLoop()
{
document.getElementById("LoopStart").value = 0; 
document.getElementById("LoopEnd").value = 248; 
}


/*function bufferSound(ctx, url) {
var p = new Promise(function(resolve, reject) {
var req = new XMLHttpRequest();
req.open('GET', url, true);
req.responseType = 'arraybuffer';
req.onload = function() {
  ctx.decodeAudioData(req.response, resolve, reject);
}
req.send();
});
return p;
}

function SetPitch(val)
{
audio.pause();
vocalOnlyAudio.pause(0;)
bufferSound(audioContext, audioSrc).then(function (buffer) {
var g = audioContext.createGain();
g.gain.value = 5;
g.connect(audioContext.destination);
var bq = audioContext.createBiquadFilter();
bq.detune.value = val;
bq.connect(g);
var src = audioContext.createBufferSource();
src.buffer = buffer;
src.connect(bq);
src.start();
});
} */
