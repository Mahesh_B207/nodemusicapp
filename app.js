var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var app = express();

// Set up routes
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var musicRouter = require('./routes/music');
var looperActivityRouter = require('./routes/looper');
var looperSessionSupportRouter = require('./routes/loopersessionsupport');
var imagesRouter = require('./routes/images');
var selftestsRouter = require('./routes/selftests');
var rhythmtestRouter = require('./routes/rhythmtest');
var mixerActivityRouter = require('./routes/mixer');
var activitiesRouter = require('./routes/activities');
var looperActivityRouter = require('./routes/looper');
var soundtracksActivityRouter = require('./routes/soundtracks');
var looperInstrumentalActivityRouter = require('./routes/looperinstrumental');
var mixermitradiActivityRouter = require('./routes/mixermitradi');
var soundtracksActivityRouter = require('./routes/soundtracks');
var soundtracksfoleyActivityRouter = require('./routes/soundtracksfoley');
var aboutusActivityRouter = require('./routes/aboutus');
var helpActivityRouter = require('./routes/help');
var acknowledgmentsActivityRouter = require('./routes/acknowledgements');
var instrumentrecognitiontestRouter = require('./routes/instrumentrecognitiontest');
var instrumentrecognitiontestsoundtracksRouter = require('./routes/instrumentrecognitiontestsoundtracks');
var mixerSessionSupportRouter = require('./routes/mixersessionsupport');
var soundtracksSessionSupportRouter = require('./routes/soundtrackssessionsupport');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Redirect to route:
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/music', musicRouter);
app.use('/activities/looper', looperActivityRouter);
app.use('/activities/looper/loopersessionsupport', looperSessionSupportRouter);
app.use('/images', imagesRouter);
app.use('/selftests', selftestsRouter);
app.use('/selftests/rhythmtest', rhythmtestRouter);
app.use('/activities/mixer', mixerActivityRouter);
app.use('/activities', activitiesRouter)
app.use('/activities/soundtracks', soundtracksActivityRouter);
app.use('/activities/looperinstrumental', looperInstrumentalActivityRouter);
app.use('/activities/mixermitradi', mixermitradiActivityRouter);
app.use('/activities/soundtracksfoley', soundtracksfoleyActivityRouter);
app.use('/aboutus', aboutusActivityRouter);
app.use('/help', helpActivityRouter);
app.use('/acknowledgements', acknowledgmentsActivityRouter);
app.use('/selftests/instrumentrecognitiontest', instrumentrecognitiontestRouter);
app.use('/selftests/instrumentrecognitiontestsoundtracks', instrumentrecognitiontestsoundtracksRouter);
app.use('/activities/mixer/mixersessionsupport', mixerSessionSupportRouter);
app.use('/activities/soundtracks/soundtrackssessionsupport', soundtracksSessionSupportRouter);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



module.exports = app;


