var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('activities', { title: 'Activities' });
});

module.exports = router;
