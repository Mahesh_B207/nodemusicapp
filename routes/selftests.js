var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('selftests', { title: 'Self Tests' });
});

module.exports = router;
