var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('rhythmtest', { title: 'Rhythm Test' });
});

module.exports = router;
