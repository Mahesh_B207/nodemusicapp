var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('mixersessionsupport', { title: 'Mixer Session Support' });
});

module.exports = router;
