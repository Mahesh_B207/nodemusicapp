var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('looperrhythmtest', { title: 'Looper Rhythm Test' });
});

module.exports = router;
