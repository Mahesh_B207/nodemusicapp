var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('instrumentrecognitiontestsoundtracks', { title: 'Instrument Recognition Test - Soundtracks' });
});

module.exports = router;
