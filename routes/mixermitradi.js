var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('mixermitradi', { title: 'Mixer - Mi Tradi' });
});

module.exports = router;
