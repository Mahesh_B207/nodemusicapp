var assert = require('assert');
Mixer = require("../views/js/mixer.js").Mixer

describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function(){
      assert.equal(-1, [1,2,3].indexOf(4));
    });
  });
});